import React from "react";

const ChangeName = ({ setUsername, username }) => {
  const handlePropChange = () => {
    //     props.changeName("there");
    setUsername("there");
  };
  return (
    <div>
      <button onClick={handlePropChange}>Hide My Name</button>
      <p>{username}, what do you have planned for today</p>
    </div>
  );
};

export default ChangeName;
