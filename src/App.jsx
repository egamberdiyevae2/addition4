import React, { useState } from "react";
import ChangeName from "./ChangeName";

const App = () => {
  const [username, setUsername] = useState("");
  const handleInput = (e) => {
    setUsername(e.target.value);
  };
  return (
    <div className="d-flex flex-column align-items-center gap-2">
      <h1>State and Props-4</h1>
      <label htmlFor="username" className="text-warning">
        Enter your name:
      </label>
      <input
        className="form-control"
        style={{ width: "300px" }}
        id="username"
        type="text"
        placeholder="My name"
        onChange={handleInput}
      />
      <p>Hi there {username}</p>
      <p className="text-success">{username}, you are doing great today</p>
      {/* <ChangeName changeName={(username) => setUsername(username)} /> */}
      <ChangeName setUsername={setUsername} username={username} />
    </div>
  );
};

export default App;
